using System.Collections;
using UnityEngine;

// Classe abstrata base para todas as armas de fogo.
public abstract class GunBase : MonoBehaviour
{
    [SerializeField] protected float fireRate;
    [SerializeField] protected int damagePerProjectile;
    [SerializeField] protected float projectileSpeed;
    [SerializeField] protected GameObject defaultProjectilePrefab; // Prefab padrão da arma. Recriada e salva no projectilePrefab.
    
    protected GameObject currentProjectileInstance; // Guarda a instância atual do tiro dado pela arma.
    protected GameObject projectilePrefab; // Prefab modificada da arma.
    protected float currentFiringTimer = 0;
    protected Coroutine firingCoroutine {get; set;}
    protected AudioSource audioSource;

    protected virtual void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        // Alterar a prefab diretamente altera TODOS os projeteis criados a partir da prefab alterada, logo,
        // é criado uma nova instância da prefab. Depois, essa instância é atualizada com os valores definidos na arma,
        // e com o layer de física adequado, definido de acordo com a tag da arma.
        projectilePrefab = Instantiate(defaultProjectilePrefab);
        ProjectileBase[] projectileScripts = projectilePrefab.GetComponentsInChildren<ProjectileBase>();

        foreach (ProjectileBase projectileScript in projectileScripts)
        {
            projectileScript.travelSpeed = projectileSpeed;
            projectileScript.damage = damagePerProjectile;
        }


        if (transform.CompareTag("Player"))
        {
            projectilePrefab.gameObject.layer = LayerMask.NameToLayer("PlayerProjectile");
        }
        else if (transform.CompareTag("Enemy"))
        {
            projectilePrefab.gameObject.layer = LayerMask.NameToLayer("EnemyProjectile");
        }   
        
        // Desativada pois ela é instânciada dentro da cena.
        projectilePrefab.SetActive(false); 
    }

    // Corotina simples que atira infinitamente, iniciada e desativada nos métodos OnEnable e OnDisable.
    protected virtual IEnumerator FiringCoroutine()
    {
        while(true)
        {
            currentFiringTimer += Time.deltaTime * fireRate;
            if (currentFiringTimer >= 1 && GameplayManager.IsObjectOnScreen(transform.position))
            {
                Fire();
                currentFiringTimer = 0;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnEnable()
    {
        if (firingCoroutine == null)
        {
            firingCoroutine = StartCoroutine(FiringCoroutine());
        }
    }

    private void OnDisable()
    {
        if (firingCoroutine != null)
        {
            StopCoroutine(firingCoroutine);
            firingCoroutine = null;
        }
    }

    private void OnDestroy()
    {
        // Ao ser destruído, destroi a instância da prefab criada.
        Destroy(projectilePrefab);
    }

    private void PreFire()
    {
        // Instância, a partir da instância da prefab, um novo projétil, e o ativa.
        Vector3 projectilePosition = transform.position;
        projectilePosition.z = defaultProjectilePrefab.transform.position.z;
        currentProjectileInstance = Instantiate(projectilePrefab, projectilePosition, transform.rotation);
        currentProjectileInstance.SetActive(true);
    }

    // Método que define a lógica de tiro, sobrescrita pelas classes concretas que a implementam.
    protected virtual void Fire()
    {
        PreFire();
        if (audioSource != null)
        {
            audioSource.Play();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.up);
    }
}
