using System.Collections;
using UnityEngine;
using GameEvents;

// Classe abstrata base para todos os tipos de ataques especiais.
public abstract class SpecialAttackBase : MonoBehaviour
{
    [SerializeField] protected int damage;

    Coroutine currentCoroutine;

    private void OnEnable()
    {
        PlayerEvents.PlayerSpecialAttack += Activate;
    }

    private void OnDisable()
    {
        PlayerEvents.PlayerSpecialAttack -= Activate;
    }
    private IEnumerator WrapperCoroutine()
    {
        currentCoroutine = StartCoroutine(BehaviourCoroutine());

        yield return currentCoroutine;

        currentCoroutine = null;
    }

    // Corotina que define a lógica do ataque especial.
    protected abstract IEnumerator BehaviourCoroutine();

    // Método que ativa o ataque especial.
    public void Activate()
    {
        if (currentCoroutine == null)
        {
            StartCoroutine(WrapperCoroutine());
        }
    }

    private void CollisionLogic(ref Collider2D other)
    {
        // Destrói projeteis inimigos caso colida com eles.
        if (other.gameObject.layer == LayerMask.NameToLayer("EnemyProjectile"))
        {
            Destroy(other.gameObject);
            return;
        }

        // Caso o objeto possua o sistema de vida, causa dano ao mesmo.
        if (other.TryGetComponent<IDamageable>(out var health))
        {
            health.GetHit(damage);
        }
    }
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        CollisionLogic(ref other);
    }
    public virtual void OnTriggerStay2D(Collider2D other)
    {
        CollisionLogic(ref other);
    }
}
