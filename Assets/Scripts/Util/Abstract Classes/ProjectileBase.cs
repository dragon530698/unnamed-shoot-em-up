using UnityEngine;

// Classe abstrata base para todos os projéteis.
public abstract class ProjectileBase : MonoBehaviour
{
    public bool destroyOnCollision = true;
    [HideInInspector]
    public float travelSpeed;
    [HideInInspector]
    public int damage;

    // Método que define a lógica do projétil, sobrescrito por classes concretas que a implementam.
    protected abstract void Behaviour();
    
    private void FixedUpdate()
    {
        // Realiza a lógica do projétil.
        Behaviour();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        // Ao colidir com qualquer coisa, primeiro é verificado se ele possui um sistema de vida (player ou inimigos).
        // Caso possua, causa dano. Caso não possua, não causa dano.
        // Em ambos os casos, o projétil é destruído.
        IDamageable hitPoints = other.gameObject.GetComponent<IDamageable>();
        if (hitPoints != null)
        {
            hitPoints.GetHit(damage);
        }
        if (destroyOnCollision)
        {
            Destroy(gameObject);
        }
    }
}
