using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Classe abstrata base para todos os coletíveis.
public abstract class CollectibleBase : MonoBehaviour
{
    [SerializeField] float speed = 2;

    AudioSource audioSource;
    bool hasActivated = false;
    protected abstract void Behaviour(Collider2D other); // Método que define o que o coletível irá fazer.

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Todos os coletíveis irão se movimentar para baixo, de acordo com a velocidade definida para cada um deles.
    private void FixedUpdate()
    {
        transform.position += Vector3.down * speed * Time.fixedDeltaTime;   
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!hasActivated)
        {
            hasActivated = true;
            
            audioSource.Play();
            Behaviour(other);
            DeactivateAndDestroy();
        }
    }

    protected void DeactivateAndDestroy()
    {
        GetComponent<Collider2D>().enabled = false;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
        Destroy(gameObject, 2);
    }
}
