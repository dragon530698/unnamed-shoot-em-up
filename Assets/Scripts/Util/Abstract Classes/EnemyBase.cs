using UnityEngine;
using GameEvents;

// Classe abstrata base para todos os inimigos.
public abstract class EnemyBase : MonoBehaviour, IDamageable
{
    [SerializeField] AudioClip onHitSfx;
    [SerializeField] GameObject deathPrefab;
    [SerializeField] int hitPoints = 3;
    [SerializeField] int scoreValue = 10;
    [SerializeField] protected float moveSpeed;

    AudioSource audioSource;
    bool isAlive = true;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        // Caso o inimigo tenha passado pela parte de baixo da tela, ele é destruído (com delay de um segundo).
        if (GameplayManager.ObjectPassedScreenBottom(transform.position))
        {
            Destroy(gameObject, 1);
        }
    }

    private void FixedUpdate()
    {
        Behaviour();
    }

    protected abstract void Behaviour();

    public virtual void PiercingHit(int damage)
    {
        GetHit(damage);
    }

    public virtual void GetHit(int damage)
    {
        hitPoints -= damage;

        if (hitPoints <= 0)
        {
            Death();
            return;
        }

        audioSource.clip = onHitSfx;
        audioSource.Play();
    }

    public void Death()
    {
        if (isAlive)
        {
            ScoreEvents.OnScoreGained(scoreValue);
            isAlive = false;
            Instantiate(deathPrefab, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
