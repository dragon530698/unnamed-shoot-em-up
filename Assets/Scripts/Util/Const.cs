public static class Const
{
    public static string HIGHSCORE_PLAYERPREF_KEY = "highscore";

    public static string BGM_VOLUME_AUDIOMIXER_KEY = "bgm_volume";
    public static string SFX_VOLUME_AUDIOMIXER_KEY = "sfx_volume";
    
    public static string BGM_MUTE_PLAYERPREFS_KEY = "bgm_mute";
    public static string SFX_MUTE_PLAYERPREFS_KEY = "sfx_mute";

    public static float DEFAULT_BGM_VOLUME = -10f;
    public static float DEFAULT_SFX_VOLUME = 0f;

}
