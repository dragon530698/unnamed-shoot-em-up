using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SpawnableData 
{
    public GameObject prefab;
    public int spawnWeight;
    public int minSpawnScore;
    public int maxSpawnScore;
    public int enemyCountsAs;
}

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnablesSO", order = 1)]
public class SpawnablesSO : ScriptableObject
{
    public List<SpawnableData> enemies;
    public List<SpawnableData> collectibles;
    public float collectibleSpawnChance;
    public Vector2 spawnDistanceFromTop;
    public float enemySpawnTickTime;
    public int scoreForExtraEnemySpawn;
    public float collectibleSpawnTickTime;
}
