using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Arma que aplica knockback ao usuário quando atira.
public class KnockbackGun : GunBase
{
    [SerializeField] float knockbackForce;
    [Range(0, 1)]
    [SerializeField] float knockbackDissipation;

    Transform parentTransform;

    protected override void Awake()
    {
        base.Awake();

        // Pega o obeto móvel mais próximo na hierarquia; Nesse caso,
        // ou é o inimigo, ou é o player.
        parentTransform = GetComponentInParent<Rigidbody2D>().transform;
    }

    IEnumerator Knockback()
    {
        // Guarda a velocidade atual em uma variável, e inicializa ela com a força de knockback
        float currentVelocity = knockbackForce;

        // Enquanto a força atual ainda for grande o suficiente para ser relevante,
        while (currentVelocity > 0.1f)
        {
            // Aplica a força, e
            parentTransform.position += -transform.up * currentVelocity * Time.fixedDeltaTime;

            // Reduz ela pela dissipação.
            currentVelocity -= currentVelocity * knockbackDissipation;
            yield return new WaitForFixedUpdate();
        }
    }

    protected override void Fire()
    {
        base.Fire();

        StartCoroutine(Knockback());
    }
}
