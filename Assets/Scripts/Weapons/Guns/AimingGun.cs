using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Arma que mira no seu alvo.
public class AimingGun : GunBase
{
    [SerializeField] float aimingRange;

    GameObject currentTarget;
    Vector3 aimingDirection;
    Vector3 defaultAimingDirection;
    int targetLayer;

    protected override void Awake()
    {
        base.Awake();

        // Salva a direção padrão da arma
        defaultAimingDirection = transform.up;

        // Pega o layer do alvo que desejamos
        if (transform.CompareTag("Enemy"))
        {
            targetLayer = LayerMask.GetMask("Player");
        }
        else if (transform.CompareTag("Player"))
        {
            targetLayer = LayerMask.GetMask("Enemy");
        }
    }

    private void FixedUpdate()
    {
        // Se não temos alvo, usamos a direção padrão
        aimingDirection = defaultAimingDirection;

        if (currentTarget == null)
        {
            // Procura um alvo, e, se for encontrado, pega a direção ao alvo
            Collider2D closestTarget = Physics2D.OverlapCircle(transform.position, aimingRange, targetLayer);
            if (closestTarget != null)
            {
                currentTarget = closestTarget.gameObject;
                AimTowardsTarget();
            }
        }
        else
        {
            AimTowardsTarget();
        }

        // Rotaciona para o alvo
        transform.rotation = Quaternion.LookRotation(Vector3.forward, aimingDirection);
    }

    private void AimTowardsTarget()
    {
        Vector3 distanceToTarget = currentTarget.transform.position - transform.position;
        distanceToTarget.z = 0;
        aimingDirection = distanceToTarget.normalized;
    }

    protected override void Fire()
    {
        base.Fire();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, aimingRange);
    }
}
