using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GunTypes
{
    BASIC,
    BOOMERANG,
    HOMING,
    MACHINEGUN,
    SHOTGUN
}

public class GunLevelSystem : MonoBehaviour
{
    public GunTypes gunType;
    public List<GameObject> gunLevels;

    GameObject currentGun;
    int currentLevel = 1;
    public int CurrentLevel
    {
        get
        {
            if (gunType == GunTypes.BASIC)
                return 0;

            return currentLevel;
        }
    }

    private void Awake()
    {
        if (gunType == GunTypes.BASIC)
            return;

        currentGun = gunLevels[currentLevel - 1];
        currentGun.SetActive(true);
    }

    public void GunLevelUp()
    {
        if (gunType == GunTypes.BASIC)
            return;

        currentLevel++;

        if (currentLevel <= gunLevels.Count)
        {
            currentGun.SetActive(false);

            currentGun = gunLevels[currentLevel - 1];
            currentGun.SetActive(true);
        }
    }
}
