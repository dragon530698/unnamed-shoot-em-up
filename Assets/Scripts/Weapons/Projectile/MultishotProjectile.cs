using System.Collections.Generic;
using UnityEngine;

// Projétil "pai" que monitora todos os projeteis filhos.
public class MultishotProjectile : ProjectileBase
{
    private void Start()
    {
        // Passa os valores desse script para os projéteis filhos.
        foreach (Transform child in transform)
        {
            child.gameObject.layer = gameObject.layer;

            ProjectileBase childProjectile = child.GetComponent<ProjectileBase>();

            childProjectile.destroyOnCollision = destroyOnCollision;
            childProjectile.damage = damage;
            childProjectile.travelSpeed = travelSpeed;
        }
    }

    protected override void Behaviour()
    {
        // Se todos os filhos forem destruídos, se destroi.
        if (transform.childCount == 0)
        {
            Destroy(gameObject);
        }
    }
}
