using UnityEngine;

// Projétil boomerang; É aplicado uma força ao ser instanciado,
// e possui uma força contrária constante sendo aplicada.
public class BoomerangProjectile : ProjectileBase
{
    [SerializeField] float counterForceCoefficient;

    Vector3 currentVelocity;

    private void Start()
    {
        currentVelocity = travelSpeed * transform.up;
    }

    protected override void Behaviour()
    {
        // Força contrária
        currentVelocity -= travelSpeed * counterForceCoefficient * Vector3.up * Time.fixedDeltaTime;

        // Aplicação da velocidade atual
        transform.position += currentVelocity * Time.fixedDeltaTime;

        // Se o projétil passou pela parte de baixo da tela, ele é destruído.
        if (GameplayManager.ObjectPassedScreenBottom(transform.position))
        {
            Destroy(gameObject);
        }
    }
}
