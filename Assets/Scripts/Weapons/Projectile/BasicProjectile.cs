using UnityEngine;

// Projétil simples. Segue em linha reta usando o eixo y LOCAL.
public class BasicProjectile : ProjectileBase
{
    protected override void Behaviour()
    {
        transform.position += travelSpeed * transform.up * Time.fixedDeltaTime;

        // Se o projétil está fora da tela, ele é destruído.
        if (!GameplayManager.IsObjectOnScreen(transform.position))
        {
            Destroy(gameObject);
        }
    }
}
