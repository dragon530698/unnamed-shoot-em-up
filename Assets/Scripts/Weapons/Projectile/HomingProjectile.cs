using UnityEngine;

// Projétil que segue o alvo mais próximo a ele.
public class HomingProjectile : ProjectileBase
{
    [Range(0, 1)]
    [SerializeField] float pullStrength;
    [SerializeField] float homingRadius;

    int targetLayer;
    Vector3 travelDirection;

    private void Awake()
    {
        travelDirection = transform.up;

        if (gameObject.layer == LayerMask.NameToLayer("EnemyProjectile"))
        {
            targetLayer = LayerMask.GetMask("Player");
        }
        else if (gameObject.layer == LayerMask.NameToLayer("PlayerProjectile"))
        {
            targetLayer = LayerMask.GetMask("Enemy");
        }
    }

    protected override void Behaviour()
    {
        Collider2D closestTarget = Physics2D.OverlapCircle(transform.position, homingRadius, targetLayer);
        if (closestTarget != null)
        {
            Vector3 distanceToTarget = closestTarget.transform.position - transform.position;
            distanceToTarget.z = 0;
            Vector3 directionToTarget = distanceToTarget.normalized;

            travelDirection = (travelDirection + directionToTarget * pullStrength).normalized;
        }

        transform.position += travelSpeed * travelDirection * Time.fixedDeltaTime;

        if (!GameplayManager.IsObjectOnScreen(transform.position))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, homingRadius);
    }
}
