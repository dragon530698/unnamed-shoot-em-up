using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using TMPro;

public class ShipSelectButton : MonoBehaviour
{
    public GameObject ship;

    [Header("Variables")]
    public bool unlockable;
    public int unlockScore;

    [Header("Object References")]
    public GameObject descriptionText;
    public GameObject unlockScoreText;

    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();

        if (unlockable)
        {
            if (PlayerPrefs.GetInt(Const.HIGHSCORE_PLAYERPREF_KEY, 0) >= unlockScore)
            {
                unlockScoreText.SetActive(false);

                descriptionText.SetActive(true);

                button.interactable = true;
            }
            else
            {
                descriptionText.SetActive(false);

                unlockScoreText.SetActive(true);
                unlockScoreText.GetComponent<TMP_Text>().text = $"Score needed:\n{unlockScore}";

                button.interactable = false;
            }
        }
    }

    public void SelectShip()
    {
        GameplayManager.Instance.SpawnPlayer(ship);
    }
}
