using System;
using UnityEngine;
using UnityEngine.Audio;

public enum AudioChannels
{
    MUSIC,
    SFX
}

// AudioMixer não permite expor a funcionalidade de mute, então,
// esse script acabou sendo mais complexo do que deveria ser.
public class AudioMuteButton : MonoBehaviour
{
    [SerializeField] AudioMixer audioMixerAsset;
    [SerializeField] AudioChannels audioChannel;

    string playerPrefMuteKey;
    string exposedAudioMixerKey;
    float defaultVolume;
    
    void Start() 
    {
        // Define os valores das variaveis de acordo com a escolha
        // do canal no editor;
        switch(audioChannel)
        {
            case AudioChannels.MUSIC:
                playerPrefMuteKey = Const.BGM_MUTE_PLAYERPREFS_KEY;
                exposedAudioMixerKey = Const.BGM_VOLUME_AUDIOMIXER_KEY;
                defaultVolume = Const.DEFAULT_BGM_VOLUME;
                break;
            case AudioChannels.SFX:
                playerPrefMuteKey = Const.SFX_MUTE_PLAYERPREFS_KEY;
                exposedAudioMixerKey = Const.SFX_VOLUME_AUDIOMIXER_KEY;
                defaultVolume = Const.DEFAULT_SFX_VOLUME;
                break;
        }
    }

    public void ToggleMute()
    {
        // Verifica se a chave existe no PlayerPrefs
        if (PlayerPrefs.HasKey(playerPrefMuteKey))
        {
            // Converte de int para bool a chave salva no playerprefs
            bool isMuted = Convert.ToBoolean(PlayerPrefs.GetInt(playerPrefMuteKey));

            if (!isMuted)
            {
                // Se não está mutado, alteramos o volume para -80db (menor valor possivel do audiomixer)
                audioMixerAsset.SetFloat(exposedAudioMixerKey, -80f);
                PlayerPrefs.SetInt(playerPrefMuteKey, 1);
            }
            else
            {
                // Se está mutado, alteramos o volume para o valor volume padrão.
                audioMixerAsset.SetFloat(exposedAudioMixerKey, defaultVolume);
                PlayerPrefs.SetInt(playerPrefMuteKey, 0);
            }
        }
        else
        {
            // Se não existe, realiza o mute normalmente.
            audioMixerAsset.SetFloat(exposedAudioMixerKey, -80f);
            PlayerPrefs.SetInt(playerPrefMuteKey, 1);
        }
    }
}
