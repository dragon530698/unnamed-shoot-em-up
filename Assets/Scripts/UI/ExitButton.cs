using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class ExitButton : MonoBehaviour
{
    public void CloseGame()
    {
        #if UNITY_EDITOR
            EditorApplication.isPlaying = false;
        #endif

        Application.Quit();
    }
}
