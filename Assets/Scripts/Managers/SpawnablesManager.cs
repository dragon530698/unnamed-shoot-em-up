using System.Collections.Generic;
using UnityEngine;

// Manager que controla o spawn de todas as entidades spawnáveis, sejam elas
// inimigos ou coletíveis.
public class SpawnablesManager : MonoBehaviour
{
    public SpawnablesSO spawnablesData;

    float screenTop, screenBottom, screenLeft, screenRight;
    float currentEnemyTimer, currentCollectibleTimer;
    int enemiesCountToSpawn;


    void Start()
    {
        // Pega a posição (do mundo) dos quatro cantos da tela.
        Vector3 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));

        screenTop = topRight.y;
        screenRight = topRight.x;
        screenBottom = bottomLeft.y;
        screenLeft = bottomLeft.x;

        // Ordena âmbas as listas de spawnaveis baseadas em seu peso de spawn, em ordem ascendente.
        spawnablesData.enemies.Sort((a, b) => a.spawnWeight.CompareTo(b.spawnWeight));
        spawnablesData.collectibles.Sort((a, b) => a.spawnWeight.CompareTo(b.spawnWeight));
    }

    void Update()
    {
        MaybeCreateSpawnables();
    }

    // Cria novos spawnáveis de acordo com o tempo de "tick" definido
    void MaybeCreateSpawnables()
    {
        if (GameplayManager.Instance.currentGameState == GameStates.PLAYING)
        {
            enemiesCountToSpawn = 1 + ScoreManager.currentScore / spawnablesData.scoreForExtraEnemySpawn;

            // Adiciona o delta do tempo á variável, e, se a variavel for maior que o tempo de "tick" definido,
            // instancia e move um novo inimigo/coletível.
            currentEnemyTimer += Time.deltaTime;
            currentCollectibleTimer += Time.deltaTime;

            if (currentEnemyTimer > spawnablesData.enemySpawnTickTime)
            {
                while(enemiesCountToSpawn > 0)
                {
                    MoveNewSpawnable(GetEnemy(ref enemiesCountToSpawn));
                }

                currentEnemyTimer = 0;
            }

            if (currentCollectibleTimer > spawnablesData.collectibleSpawnTickTime)
            {
                if (Random.Range(0.0f, 100.0f) > 100.0f - spawnablesData.collectibleSpawnChance)
                {
                    MoveNewSpawnable(GetCollectible());
                }
                currentCollectibleTimer = 0;
            }
        }
    }

    void MoveNewSpawnable(GameObject spawnable)
    {
        // Retorna se o spawnável atual for nulo.
        if (spawnable == null) return;

        // Adiciona o spawnável instanciado como filho.
        spawnable.transform.parent = transform;

        // Cálculo das novas bordas para ser movido a instância, levando em
        // consideração o seu sprite.
        float actualLeftBorder = screenLeft;
        float actualRightBorder = screenRight;

        Bounds newSpawnableBounds = spawnable.GetComponentInChildren<SpriteRenderer>().bounds;
        if (newSpawnableBounds != null)
        {
            actualLeftBorder += newSpawnableBounds.extents.x;
            actualRightBorder -= newSpawnableBounds.extents.x;
        }

        // Escolhe uma posição entre os dois lados da tela, levando em consideração
        // o sprite do spawnável, e o posiciona de acordo.
        Vector2 finalLocalPosition = Vector2.zero;
        finalLocalPosition.x = Random.Range(actualLeftBorder, actualRightBorder);
        finalLocalPosition.y = screenTop + Random.Range(spawnablesData.spawnDistanceFromTop.x, spawnablesData.spawnDistanceFromTop.y);
        spawnable.transform.localPosition = finalLocalPosition;
    }

    // Weighted random numbers - https://stackoverflow.com/questions/1761626/weighted-random-numbers
    GameObject GetEnemy(ref int currentEnemyCount)
    {
        // Seleciona os candidatos para spawn e calcula a soma dos pesos
        List<SpawnableData> spawnCandidates = GetSpawnCandidates(spawnablesData.enemies, out int weightSum);

        // Gera um numero aleatorio
        int rnd = Random.Range(0, weightSum);

        // Para cada candidato, verifica se o valor gerado é menor que o peso de spawn do spawnável.
        // Caso seja, retorna o objeto instanciado; Caso contrário, reduz do valor gerado o peso do spawnável,
        // e passa para a próxima iteração.
        foreach (SpawnableData spawnable in spawnCandidates)
        {
            if (rnd < spawnable.spawnWeight)
            {
                currentEnemyCount -= spawnable.enemyCountsAs;
                return Instantiate(spawnable.prefab);
            }
            rnd -= spawnable.spawnWeight;
        }

        // Nunca irá chegar aqui.
        return null;
    }

    GameObject GetCollectible()
    {
        // Seleciona os candidatos para spawn e calcula a soma dos pesos
        List<SpawnableData> spawnCandidates = GetSpawnCandidates(spawnablesData.collectibles, out int weightSum);

        // Gera um numero aleatorio
        int rnd = Random.Range(0, weightSum);

        // Para cada candidato, verifica se o valor gerado é menor que o peso de spawn do spawnável.
        // Caso seja, retorna o objeto instanciado; Caso contrário, reduz do valor gerado o peso do spawnável,
        // e passa para a próxima iteração.
        foreach (SpawnableData spawnable in spawnCandidates)
        {
            if (rnd < spawnable.spawnWeight)
            {
                return Instantiate(spawnable.prefab);
            }
            rnd -= spawnable.spawnWeight;
        }

        // Nunca irá chegar aqui.
        return null;
    }

    List<SpawnableData> GetSpawnCandidates(List<SpawnableData> spawnablesList, out int weightSum)
    {
        List<SpawnableData> spawnCandidates = new List<SpawnableData>();
        weightSum = 0;

        foreach (SpawnableData spawnable in spawnablesList)
        {
            if (ScoreManager.currentScore >= spawnable.minSpawnScore
            && ScoreManager.currentScore <= spawnable.maxSpawnScore)
            {
                spawnCandidates.Add(spawnable);
                weightSum += spawnable.spawnWeight;
            }
        }

        return spawnCandidates;
    }
}
