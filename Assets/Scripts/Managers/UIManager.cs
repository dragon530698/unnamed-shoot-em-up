using UnityEngine;
using TMPro;
using GameEvents;

// Execution order maior do que o normal, para garantir que o script sera executado DEPOIS do player.
[DefaultExecutionOrder(9)]

// Manager que controla todos os objetos de texto.
public class UIManager : MonoBehaviour
{
    PlayerController player;

    public GameObject shipSelectMenu;
    public GameObject pauseMenu;
    public GameObject prePlayMenu;
    public GameObject gameOverMenu;
    public TMP_Text scoreLabel;
    public TMP_Text chargesLabel;
    public TMP_Text gameoverScoreLabel;
    public TMP_Text gameoverHighscoreLabel;
    public TMP_Text gameoverNewHighscoreLabel;

    private void OnEnable()
    {
        ScoreEvents.ScoreGained += UpdateScoreUI;

        PlayerEvents.PlayerSpecialAttack += UpdateChargesUI;
        PlayerEvents.SpecialChargeGained += UpdateChargesUI;
        PlayerEvents.PlayerDeath += OnGameOver;

        GameplayEvents.ShipSelected += OnShipSelected;
        GameplayEvents.GameStart += OnGameStart;
        GameplayEvents.GamePauseToggle += OnGamePauseToggle;
    }

    private void OnDisable()
    {
        ScoreEvents.ScoreGained -= UpdateScoreUI;

        PlayerEvents.PlayerSpecialAttack -= UpdateChargesUI;
        PlayerEvents.SpecialChargeGained -= UpdateChargesUI;
        PlayerEvents.PlayerDeath -= OnGameOver;

        GameplayEvents.ShipSelected -= OnShipSelected;
        GameplayEvents.GameStart -= OnGameStart;
        GameplayEvents.GamePauseToggle -= OnGamePauseToggle;
    }

    public void OnShipSelected(GameObject player)
    {
        this.player = player.GetComponent<PlayerController>();

        shipSelectMenu.SetActive(false);
        prePlayMenu.SetActive(true);
    }

    public void OnGamePauseToggle()
    {
        if (GameplayManager.Instance.currentGameState == GameStates.PREPLAY
        || GameplayManager.Instance.currentGameState == GameStates.GAMEOVER) return;
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    public void OnGameOver()
    {
        if (GameplayManager.Instance.currentGameState == GameStates.GAMEOVER) return;

        ToggleGameplayUI();

        gameOverMenu.SetActive(true);
        gameoverScoreLabel.text = $"Score:\n{ScoreManager.currentScore}";

        gameoverNewHighscoreLabel.gameObject.SetActive(false);

        if (ScoreManager.CheckForNewHighscore())
        {
            gameoverNewHighscoreLabel.gameObject.SetActive(true);
        }

        gameoverHighscoreLabel.text = $"Highscore:\n{ScoreManager.highscore}";
    }

    public void OnGameStart()
    {
        if (GameplayManager.Instance.currentGameState != GameStates.PREPLAY) return;
        prePlayMenu.SetActive(false);
        ToggleGameplayUI();
        UpdateScoreUI();
        UpdateChargesUI();
    }

    public void ToggleGameplayUI()
    {
        scoreLabel.gameObject.SetActive(!scoreLabel.gameObject.activeSelf);
        chargesLabel.gameObject.SetActive(!chargesLabel.gameObject.activeSelf);
    }

    public void UpdateScoreUI(int _ = 0)
    {
        if (GameplayManager.Instance.currentGameState == GameStates.GAMEOVER) return;

        if (scoreLabel != null)
        {
            scoreLabel.text = $"Score: {ScoreManager.currentScore}";
        }
    }

    public void UpdateChargesUI()
    {
        if (GameplayManager.Instance.currentGameState == GameStates.GAMEOVER)
            return;

        if (chargesLabel != null)
        {
            chargesLabel.text = $"Special Charges: {player.GetSpecialChargesCount()}";
        }
    }
}
