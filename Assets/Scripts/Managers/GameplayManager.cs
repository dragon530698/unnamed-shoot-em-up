using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using GameEvents;

public enum GameStates
{
    PLAYING = 0,
    PAUSED = 1,
    PREPLAY = 2,
    GAMEOVER = 3,
    CHARSELECT = 4
}

// Manager que controla a gameplay e providencia alguns métodos utilitários.
[DefaultExecutionOrder(10)]
public class GameplayManager : Singleton<GameplayManager>
{
    public GameStates currentGameState = GameStates.CHARSELECT;

    // Retorna verdadeiro se o ponto é visível pela câmera, e falso caso não seja.
    public static bool IsObjectOnScreen(Vector3 worldPosition)
    {
        Vector3 viewportPosition = Camera.main.WorldToViewportPoint(worldPosition);

        if (viewportPosition.x < 0 || viewportPosition.x > 1
            || viewportPosition.y < 0 || viewportPosition.y > 1)
        {
            return false;
        }

        return true;
    }

    // Retorna se o objeto saiu pela tela pela parte de baixo (viewport.y < 0).
    public static bool ObjectPassedScreenBottom(Vector3 worldPosition)
    {
        Vector3 viewportPosition = Camera.main.WorldToViewportPoint(worldPosition);

        if (viewportPosition.y < 0) return true;

        return false;
    }

    protected override void Awake()
    {
        base.Awake();

        currentGameState = GameStates.CHARSELECT;
        PlayerEvents.PlayerDeath += Instance.OnGameOver;
        GameplayEvents.GameStart += Instance.OnGameStart;
        GameplayEvents.GamePauseToggle += Instance.OnGamePauseToggle;
    }

    private void OnApplicationQuit()
    {
        GameplayEvents.GameStart -= Instance.OnGameStart;
        GameplayEvents.GamePauseToggle -= Instance.OnGamePauseToggle;
        PlayerEvents.PlayerDeath -= Instance.OnGameOver;
    }

    public void SpawnPlayer(GameObject playerShipPrefab)
    {
        // Lê a posição do mouse atual (em posição de tela)
        Vector2 mousePos = Mouse.current.position.ReadValue();

        // Limita a posição do mouse á tela
        Vector3 mouseViewportPos = Camera.main.ScreenToViewportPoint(mousePos);
        mouseViewportPos.x = Mathf.Clamp01(mouseViewportPos.x);
        mouseViewportPos.y = Mathf.Clamp01(mouseViewportPos.y);

        Vector3 spawnPos = Camera.main.ViewportToWorldPoint(mouseViewportPos);
        spawnPos.z = 0;

        GameObject newPlayer = Instantiate(playerShipPrefab, spawnPos, Quaternion.identity);

        currentGameState = GameStates.PREPLAY;
        GameplayEvents.OnShipSelected(newPlayer);
    }

    void OnGameStart()
    {
        if (currentGameState == GameStates.PREPLAY)
        {
            Cursor.lockState = CursorLockMode.Confined;
            currentGameState = GameStates.PLAYING;
        }
        else if (currentGameState == GameStates.GAMEOVER)
        {
            PlayerEvents.PlayerDeath -= Instance.OnGameOver;
            GameplayEvents.GameStart -= Instance.OnGameStart;
            GameplayEvents.GamePauseToggle -= Instance.OnGamePauseToggle;

            currentGameState = GameStates.PREPLAY;
            SceneManager.LoadScene(0);
        }
    }

    void OnGameOver()
    {
        Cursor.lockState = CursorLockMode.None;
        currentGameState = GameStates.GAMEOVER;
    }

    void OnGamePauseToggle()
    {
        if (currentGameState == GameStates.PREPLAY
        || currentGameState == GameStates.GAMEOVER
        || currentGameState == GameStates.CHARSELECT) return;

        Cursor.lockState = Cursor.lockState == CursorLockMode.Confined ? CursorLockMode.None : CursorLockMode.Confined;
        Time.timeScale = 1 - Time.timeScale; // Se timescale for 0, retorna 1, caso seja 1, retorna 0.
        currentGameState = 1 - currentGameState;
    }
}
