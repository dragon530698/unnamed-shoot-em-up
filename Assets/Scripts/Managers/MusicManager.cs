using System;
using UnityEngine;
using UnityEngine.Audio;

// Classe herdeira de uma singleton,
// para a música não reiniciar ao recarregar a cena.
public class MusicManager : Singleton<MusicManager>
{
    [SerializeField] AudioMixer audioMixerAsset;

    protected override void Awake()
    {
        base.Awake();

        LoadVolumes();
    }

    void LoadVolumes()
    {
        LoadChannelVolume(Const.BGM_MUTE_PLAYERPREFS_KEY, Const.BGM_VOLUME_AUDIOMIXER_KEY);
        LoadChannelVolume(Const.SFX_MUTE_PLAYERPREFS_KEY, Const.SFX_VOLUME_AUDIOMIXER_KEY);
    }
    
    void LoadChannelVolume(string playerPrefMuteKey, string exposedAudioMixerKey)
    {
        // Verifica se a chave existe no PlayerPrefs
        if (PlayerPrefs.HasKey(playerPrefMuteKey))
        {
            // Converte de int para bool a chave salva no playerprefs
            bool isMuted = Convert.ToBoolean(PlayerPrefs.GetInt(playerPrefMuteKey));

            if (isMuted)
            {
                // Se está mutado, alteramos o volume de acordo.
                audioMixerAsset.SetFloat(exposedAudioMixerKey, -80f);
            }
        }
    }
}
