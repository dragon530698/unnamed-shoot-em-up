using UnityEngine;
using UnityEngine.Events;

// Namespace que centraliza todos os eventos do jogo.
namespace GameEvents
{
    public static class PlayerEvents
    {
        public static event UnityAction PlayerHit;
        public static void OnPlayerHit() => PlayerHit?.Invoke();

        public static event UnityAction PlayerPiercingHit;
        public static void OnPlayerPiercingHit() => PlayerPiercingHit?.Invoke();

        public static event UnityAction PlayerDeath;
        public static void OnPlayerDeath() => PlayerDeath?.Invoke();

        public static event UnityAction<GameObject> PlayerGunPickup;
        public static void OnPlayerGunPickup(GameObject newGun) => PlayerGunPickup?.Invoke(newGun);

        public static event UnityAction PlayerSpecialAttackInput;
        public static void OnPlayerSpecialAttackInput() => PlayerSpecialAttackInput?.Invoke();

        public static event UnityAction PlayerSpecialAttack;
        public static void OnPlayerSpecialAttack() => PlayerSpecialAttack?.Invoke();

        public static event UnityAction PlayerSpecialAttackEnd;
        public static void OnPlayerSpecialAttackEnd() => PlayerSpecialAttackEnd?.Invoke();

        public static event UnityAction<bool> PlayerDefenseInput;
        public static void OnPlayerDefenseInput(bool started) => PlayerDefenseInput?.Invoke(started);

        public static event UnityAction SpecialChargeGained;
        public static void OnSpecialChargeGained() => SpecialChargeGained?.Invoke();
    }

    public static class ScoreEvents
    {
        public static event UnityAction<int> ScoreGained;
        public static void OnScoreGained(int value) => ScoreGained?.Invoke(value);
    }

    public static class GameplayEvents
    {
        public static event UnityAction<GameObject> ShipSelected;
        public static void OnShipSelected(GameObject player) => ShipSelected?.Invoke(player);

        public static event UnityAction GameStart;
        public static void OnGameStart() => GameStart?.Invoke();

        public static event UnityAction GamePauseToggle;
        public static void OnGamePauseToggle() => GamePauseToggle?.Invoke();
    }
}