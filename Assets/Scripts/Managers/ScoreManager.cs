using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using GameEvents;

// Manager que controla a pontuação do jogo.
public class ScoreManager : MonoBehaviour
{
    public static int highscore {
        get
        {
            if (PlayerPrefs.HasKey(Const.HIGHSCORE_PLAYERPREF_KEY))
            {
                return PlayerPrefs.GetInt(Const.HIGHSCORE_PLAYERPREF_KEY);
            }
            else
            {
                return currentScore;
            }
        }
    }
    public static int currentScore = 0;
    public TMP_Text scoreLabel;

    public static bool CheckForNewHighscore()
    {
        if (PlayerPrefs.HasKey(Const.HIGHSCORE_PLAYERPREF_KEY))
        {
            if (PlayerPrefs.GetInt(Const.HIGHSCORE_PLAYERPREF_KEY) < ScoreManager.currentScore)
            {
                PlayerPrefs.SetInt(Const.HIGHSCORE_PLAYERPREF_KEY, ScoreManager.currentScore);
                return true;
            }

            return false;
        }
        else
        {
            PlayerPrefs.SetInt(Const.HIGHSCORE_PLAYERPREF_KEY, ScoreManager.currentScore);
            return true;
        }
    }

    private void OnEnable()
    {
        ScoreEvents.ScoreGained += AddToScore;
    }

    private void OnDisable()
    {
        ScoreEvents.ScoreGained -= AddToScore;
        currentScore = 0;
    }

    public void AddToScore(int value)
    {
        currentScore += value;
    }
}
