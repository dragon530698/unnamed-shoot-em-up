using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inimigo simples que se move em linha reta para baixo.
public class FollowerEnemy : EnemyBase
{
    [Range(0, 1)]
    [SerializeField] float followStrength;
    [SerializeField] float followRadius;

    Vector3 moveDirection;
    int playerLayer;

    private void Start()
    {
        playerLayer = LayerMask.GetMask("Player");
    }

    protected override void Behaviour()
    {
        Collider2D closestTarget = Physics2D.OverlapCircle(transform.position, followRadius, playerLayer);
        if (closestTarget != null)
        {
            Vector3 distanceToTarget = closestTarget.transform.position - transform.position;
            distanceToTarget.z = 0;
            Vector3 directionToTarget = distanceToTarget.normalized;

            moveDirection = (moveDirection + directionToTarget * followStrength).normalized;
        }

        transform.position += moveDirection * moveSpeed * Time.fixedDeltaTime;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, followRadius);
    }
}
