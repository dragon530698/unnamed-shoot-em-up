using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inimigo simples que se move em linha reta para baixo.
public class BasicEnemy : EnemyBase
{
    protected override void Behaviour()
    {
        transform.position += Vector3.down * moveSpeed * Time.fixedDeltaTime;
    }
}
