using UnityEngine;
using GameEvents;

// Colletível de cargas do ataque especial.
public class SpecialChargeCollectible : CollectibleBase
{
    protected override void Behaviour(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
            other.GetComponent<PlayerController>()?.GainSpecialAttackCharge();
    }
}
