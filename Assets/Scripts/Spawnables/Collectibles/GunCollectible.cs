using UnityEngine;
using GameEvents;

// Colletível de armas.
public class GunCollectible : CollectibleBase
{
    [SerializeField] GameObject gun;

    protected override void Behaviour(Collider2D _)
    {
        PlayerEvents.OnPlayerGunPickup(gun);
    }
}
