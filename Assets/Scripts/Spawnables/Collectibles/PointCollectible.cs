using UnityEngine;
using GameEvents;

// Colletível de pontos.
public class PointCollectible : CollectibleBase
{
    [SerializeField] int pointValue = 100;

    protected override void Behaviour(Collider2D _)
    {
        ScoreEvents.OnScoreGained(pointValue);
    }
}
