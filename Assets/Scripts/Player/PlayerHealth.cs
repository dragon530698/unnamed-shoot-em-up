using UnityEngine;
using GameEvents;

// Simples sistema que toma conta da lógica de levar dano e morte do player.
public class PlayerHealth : MonoBehaviour, IDamageable
{
    bool defending;

    private void OnEnable()
    {
        PlayerEvents.PlayerDefenseInput += ControlDefense;
    }

    private void OnDisable()
    {
        PlayerEvents.PlayerDefenseInput -= ControlDefense;
    }

    public void PiercingHit(int damage)
    {
        PlayerEvents.OnPlayerHit();

        Death();
    }

    public void GetHit(int damage)
    {
        if (!defending)
        {
            Death();
            return;
        }
        
        PlayerEvents.OnPlayerHit();
    }

    public void Death()
    {
        PlayerEvents.OnPlayerDeath();
    }

    public void ControlDefense(bool defenseState)
    {
        defending = defenseState;
    }
}
