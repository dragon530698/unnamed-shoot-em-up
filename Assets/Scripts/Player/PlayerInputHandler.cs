using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using GameEvents;

public class PlayerInputHandler : MonoBehaviour
{
    PlayerInput playerInput;
    InputActionMap inGameMap;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        inGameMap = playerInput.actions.FindActionMap("In-Game");
    }

    private void OnEnable()
    {
        // Conexão de métodos aos eventos de input, e ativação do input system.
        inGameMap["Shield"].started += OnShieldInput;
        inGameMap["Shield"].canceled += OnShieldInput;

        inGameMap["Pause"].performed += OnPauseGameInput;

        inGameMap["StartGame"].performed += OnGameStartInput;

        inGameMap["Laser"].started += OnSpecialAttackInput;

        inGameMap.Enable();
    }

    private void OnDisable()
    {
        // Remoção da conexão de métodos aos eventos de input, e desativação do input system.
        inGameMap["Shield"].started -= OnShieldInput;
        inGameMap["Shield"].canceled -= OnShieldInput;

        inGameMap["Pause"].performed -= OnPauseGameInput;

        inGameMap["StartGame"].performed -= OnGameStartInput;

        inGameMap["Laser"].performed -= OnSpecialAttackInput;

        inGameMap.Disable();
    }

    public void OnPauseGameInput(InputAction.CallbackContext _)
    {
        GameplayEvents.OnGamePauseToggle();
    }

    public void OnGameStartInput(InputAction.CallbackContext _)
    {
        GameplayEvents.OnGameStart();
    }

    public void OnShieldInput(InputAction.CallbackContext ctx)
    {
        PlayerEvents.OnPlayerDefenseInput(ctx.started);
    }

    public void OnSpecialAttackInput(InputAction.CallbackContext _)
    {
        PlayerEvents.OnPlayerSpecialAttackInput();
    }
}
