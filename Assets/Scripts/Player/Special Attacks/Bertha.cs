using System.Collections;
using UnityEngine;
using GameEvents;

public class Bertha : SpecialAttackBase
{
    [SerializeField] BerthaProjectile berthaProjectilePrefab;
    Animator animator;
    AudioSource audioSource;

    BerthaProjectile berthaProjectileInstance;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    protected override IEnumerator BehaviourCoroutine()
    {
        PlayerEvents.OnPlayerDefenseInput(true);

        animator.SetTrigger("Activate");
        audioSource.Play();

        Vector3 projectilePosition = transform.position;
        // projectilePosition.z = berthaProjectilePrefab.transform.position.z;

        berthaProjectileInstance = Instantiate(berthaProjectilePrefab, projectilePosition, transform.rotation);
        berthaProjectileInstance.ProjectileDestroyed += OnProjectileDestroyed;

        while (berthaProjectileInstance != null)
        {
            yield return new WaitForEndOfFrame();
        }
    }

    void OnProjectileDestroyed()
    {
        if (berthaProjectileInstance != null)
            berthaProjectileInstance.ProjectileDestroyed -= OnProjectileDestroyed;

        animator.SetTrigger("End");
        PlayerEvents.OnPlayerSpecialAttackEnd();
        PlayerEvents.OnPlayerDefenseInput(false);
        berthaProjectileInstance = null;
    }
}
