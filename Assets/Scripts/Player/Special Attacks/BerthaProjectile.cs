using System.Collections;
using UnityEngine;
using GameEvents;
using UnityEngine.Events;

public class BerthaProjectile : SpecialAttackBase
{
    public UnityAction ProjectileDestroyed;
    [SerializeField] float speed = 4f;
    [Range(0, 1)]
    [SerializeField] float autoDetonationScreenHeight = 0.9f;
    Animator animator;
    AudioSource audioSource;
    bool hasExploded = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        // PlayerEvents.PlayerSpecialAttackInput += Explode;

        Activate();
    }

    private void FixedUpdate()
    {
        if (hasExploded) return;

        transform.position += Vector3.up * speed * Time.fixedDeltaTime;
    }

    // private void OnDestroy()
    // {
    //     PlayerEvents.PlayerSpecialAttackInput -= Explode;
    // }

    protected override IEnumerator BehaviourCoroutine()
    {
        int travelClip = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;

        while (animator.GetCurrentAnimatorStateInfo(0).shortNameHash == travelClip)
        {
            if (Camera.main.WorldToViewportPoint(transform.position).y >= autoDetonationScreenHeight)
            {
                Explode();
            }
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f);
        }
    }

    private void Explode()
    {
        if (hasExploded) return;

        animator.SetTrigger("Explode");
        audioSource.Play();
        hasExploded = true;
    }

    public void DestroySelfAnimationEvent()
    {
        ProjectileDestroyed?.Invoke();
        Destroy(gameObject);
    }
}
