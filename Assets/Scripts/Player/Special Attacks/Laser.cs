using System.Collections;
using UnityEngine;
using GameEvents;

public enum LaserSFX
{
    STARTUP,
    FIRE
}

public class Laser : SpecialAttackBase
{
    [SerializeField] AudioClip startupAudioClip, fireAudioClip;
    Animator animator;
    AudioSource audioSource;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    protected override IEnumerator BehaviourCoroutine()
    {
        PlayerEvents.OnPlayerDefenseInput(true);
        int idleClip = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        animator.SetTrigger("Activate");

        yield return new WaitWhile(() => { return animator.GetCurrentAnimatorStateInfo(0).shortNameHash == idleClip;});

        // Enquanto o clipe atual do animador for diferente do clip de espera,
        // aguarda a animação acabar...
        while (animator.GetCurrentAnimatorStateInfo(0).shortNameHash != idleClip)
        {
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f);
        }

        // ...para então depois enviar o sinal de que o especial acabou.
        PlayerEvents.OnPlayerSpecialAttackEnd();
        PlayerEvents.OnPlayerDefenseInput(false);
    }

    public void PlaySFXAnimationEvent(LaserSFX sfx)
    {
        switch(sfx)
        {
            case LaserSFX.STARTUP:
                audioSource.clip = startupAudioClip;
                break;
            case LaserSFX.FIRE:
                audioSource.clip = fireAudioClip;
                break;
        }

        audioSource.Play();
    }
}
