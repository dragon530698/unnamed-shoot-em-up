using System.Collections;
using UnityEngine;
using GameEvents;

public class HomingSalvo : SpecialAttackBase
{
    [SerializeField] GameObject gunsHolder;
    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    protected override IEnumerator BehaviourCoroutine()
    {
        Debug.Log("Starting!");
        gunsHolder.SetActive(true);
        PlayerEvents.OnPlayerDefenseInput(true);

        int idleClip = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        animator.SetTrigger("Activate");

        yield return new WaitWhile(() => { return animator.GetCurrentAnimatorStateInfo(0).shortNameHash == idleClip;});

        // Enquanto o clipe atual do animador for diferente do clip de espera,
        // aguarda a animação acabar...
        while (animator.GetCurrentAnimatorStateInfo(0).shortNameHash != idleClip)
        {
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f);
        }

        PlayerEvents.OnPlayerSpecialAttackEnd();
        PlayerEvents.OnPlayerDefenseInput(false);
        gunsHolder.SetActive(false);
        Debug.Log("Ended!");
    }
}
