using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using GameEvents;

enum PlayerStates
{
    IDLE,
    DEFENSE,
    SPECIAL,
    DEAD
}

public class PlayerController : MonoBehaviour
{
    #region variables
    [Header("Variables")]

    [Tooltip("How far away, HORIZONTALLY, from the border, should the player be, if he tries to go out of screen? x = min, y = max.\n\n"
    + "Values lower than zero or higher than one results in the player going off-screen, and incorrectly setting these can break gameplay.")]
    [SerializeField] Vector2 screenHorizontalPadding = new Vector2(0, 1);
    [Tooltip("How far away, VERTICALLY, from the border, should the player be, if he tries to go out of screen? x = min, y = max.\n\n"
    + "Values lower than zero or higher than one results in the player going off-screen, and incorrectly setting these can break gameplay.")]
    [SerializeField] Vector2 screenVerticalPadding = new Vector2(0, 1);
    [SerializeField] float moveSpeed = 5;
    [SerializeField] int maxSpecialCharges = 3;
    [SerializeField] int maxGuns = 1;
    [SerializeField] bool toggleDefenseInput = true;
    [SerializeField] bool toggleSpecialGODeactivation = true;

    [Header("Object references, DO NOT TOUCH!")]
    [SerializeField] GameObject special;
    [SerializeField] GameObject gunSlot;
    [SerializeField] GameObject deathChunksPrefab;

    PlayerStates playerState = PlayerStates.IDLE;
    AudioSource audioSource;
    int specialCharges;
    Vector3 currentVelocity = Vector3.zero;
    Vector3 targetPos = Vector3.zero;
    #endregion


    #region unity event functions
    private void Awake()
    {
        specialCharges = maxSpecialCharges;

        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        // Conexão de métodos aos seus eventos.
        PlayerEvents.PlayerDefenseInput += OnDefenseInput;
        PlayerEvents.PlayerSpecialAttackInput += OnSpecialAttackInput;
        PlayerEvents.PlayerSpecialAttackEnd += OnSpecialAttackEnd;
        PlayerEvents.PlayerGunPickup += SwitchGuns;
        PlayerEvents.PlayerDeath += Death;
    }

    // NOTE: A remoção das conexões aos eventos é importante para evitar bugs imprevistos causados por
    // conexões que possivelmente possam não ter sido desfeitas.
    private void OnDisable()
    {
        // Remoção da conexão de métodos aos seus eventos.
        PlayerEvents.PlayerDefenseInput -= OnDefenseInput;
        PlayerEvents.PlayerSpecialAttackInput -= OnSpecialAttackInput;
        PlayerEvents.PlayerSpecialAttackEnd -= OnSpecialAttackEnd;
        PlayerEvents.PlayerGunPickup -= SwitchGuns;
        PlayerEvents.PlayerDeath -= Death;
    }

    private void Update()
    {
        if (playerState == PlayerStates.DEAD) return;

        // Lê a posição do mouse atual (em posição de tela)
        Vector2 mousePos = Mouse.current.position.ReadValue();

        // Limita a posição do mouse á tela, levando em consideração as margens definidas pelas variaveis.
        Vector3 mouseViewportPos = Camera.main.ScreenToViewportPoint(mousePos);
        mouseViewportPos.x = Mathf.Clamp(mouseViewportPos.x, screenHorizontalPadding.x, screenHorizontalPadding.y);
        mouseViewportPos.y = Mathf.Clamp(mouseViewportPos.y, screenVerticalPadding.x, screenVerticalPadding.y);

        targetPos = Camera.main.ViewportToWorldPoint(mouseViewportPos);
        targetPos.z = 0;

        HandlePlayerStates();
    }

    private void FixedUpdate()
    {
        if (playerState == PlayerStates.DEAD) return;

        // Movimenta o player para a posição do mouse.
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref currentVelocity, 1 / moveSpeed);
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (playerState == PlayerStates.DEAD) return;

        if (other.gameObject.TryGetComponent<IDamageable>(out var health))
        {
            // Meio chato quando você usa um especial e morre, né?
            if (playerState != PlayerStates.SPECIAL)
            {
                health.PiercingHit(99);
            }
            GetComponent<IDamageable>().PiercingHit(99);
        }
    }
    #endregion


    #region class methods
    public int GetSpecialChargesCount()
    {
        return specialCharges;
    }

    // Desativa ou ativa objetos baseados no estado atual do player.
    void HandlePlayerStates()
    {
        switch (playerState)
        {
            case PlayerStates.IDLE:
                gunSlot.SetActive(true);
                break;
            case PlayerStates.SPECIAL:
            case PlayerStates.DEFENSE:
                gunSlot.SetActive(false);
                break;
        }
    }

    public void Death()
    {
        if (playerState == PlayerStates.DEAD) return;

        playerState = PlayerStates.DEAD;
        // Destroi todos os filhos do player, e instancia suas "death chunks" (prefab, consiste de peças quebradas da nave)
        Instantiate(deathChunksPrefab, transform.position, Quaternion.identity);
        GetComponent<Collider2D>().enabled = false;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    // Destroi todas as armas no gunSlot, e instancia nova arma de acordo com a arma passada.
    public void SwitchGuns(GameObject newGunObject)
    {
        GunLevelSystem newGunLevelSystem = newGunObject.GetComponent<GunLevelSystem>();
        GunLevelSystem[] currentGunSystems = gunSlot.GetComponentsInChildren<GunLevelSystem>();

        GunLevelSystem lowestLevelGunSystem = null;
        foreach (GunLevelSystem gunSystem in currentGunSystems)
        {
            if (newGunLevelSystem != null && gunSystem != null)
            {
                if (newGunLevelSystem.gunType == gunSystem.gunType)
                {
                    gunSystem.GunLevelUp();
                    return;
                }

                if (lowestLevelGunSystem == null || gunSystem.CurrentLevel < lowestLevelGunSystem.CurrentLevel)
                {
                    lowestLevelGunSystem = gunSystem;
                }
            }
        }

        if (currentGunSystems.Length >= maxGuns)
        {
            if (lowestLevelGunSystem != null)
                Destroy(lowestLevelGunSystem.gameObject);
            else if (currentGunSystems != null && currentGunSystems.Length > 0 && currentGunSystems[0] != null)
                Destroy(currentGunSystems[0].gameObject);
        }

        newGunObject.tag = gameObject.tag;
        Instantiate(newGunObject, transform.position, Quaternion.identity, gunSlot.transform);
    }

    public void GainSpecialAttackCharge()
    {
        specialCharges = Mathf.Clamp(specialCharges + 1, 0, maxSpecialCharges);
        PlayerEvents.OnSpecialChargeGained();
    }

    public void OnSpecialAttackInput()
    {
        if (GameplayManager.Instance.currentGameState != GameStates.PLAYING)
            return;

        if (playerState != PlayerStates.IDLE)
            return;

        if (specialCharges <= 0)
            return;

        specialCharges--;
        playerState = PlayerStates.SPECIAL;
        if (toggleSpecialGODeactivation)
        {
            special.SetActive(true);
        }
        PlayerEvents.OnPlayerSpecialAttack();
        // PlayerEvents.OnSpecialChargeGained();
    }

    public void OnSpecialAttackEnd()
    {
        if (toggleSpecialGODeactivation)
        {
            special.SetActive(false);
        }
        playerState = PlayerStates.IDLE;
    }

    public void OnDefenseInput(bool started)
    {
        if (playerState == PlayerStates.DEAD)
            return;

        if (playerState == PlayerStates.SPECIAL)
            return;

        if (GameplayManager.Instance.currentGameState != GameStates.PLAYING)
            return;

        if (!toggleDefenseInput)
            return;

        playerState = started ? PlayerStates.DEFENSE : PlayerStates.IDLE;
        // if (started)
        // {
        //     playerState = PlayerStates.DEFENSE;
        // }
        // else
        // {
        //     playerState = PlayerStates.IDLE;
        // }
    }
    #endregion
}
