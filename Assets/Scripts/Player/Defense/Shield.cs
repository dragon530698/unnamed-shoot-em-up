using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;

public class Shield : MonoBehaviour
{
    [SerializeField] AudioClip shieldHitSfx;
    [SerializeField] AudioClip shieldRaiseSfx;
    GameObject shieldGO;

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        shieldGO = transform.GetChild(0).gameObject;
    }

    private void OnEnable()
    {
        PlayerEvents.PlayerHit += OnPlayerHit;
        PlayerEvents.PlayerDefenseInput += ControlShield;
    }

    private void OnDisable()
    {
        PlayerEvents.PlayerHit -= OnPlayerHit;
        PlayerEvents.PlayerDefenseInput -= ControlShield;
    }

    public void ControlShield(bool state)
    {
        shieldGO.SetActive(state);
        if (state)
        {
            audioSource.clip = shieldRaiseSfx;
            audioSource.Play();
        }
    }

    public void OnPlayerHit()
    {
        audioSource.clip = shieldHitSfx;
        audioSource.Play();
    }
}
