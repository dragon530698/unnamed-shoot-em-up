using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;

public class Phase : MonoBehaviour
{
    [SerializeField] AudioClip phaseDodgeSFX;
    [SerializeField] AudioClip phaseStartSfx;
    [SerializeField] Color phaseColor;

    [Header("Object References")]
    [SerializeField] SpriteRenderer shipGraphics;

    Collider2D[] playerColliders;
    AudioSource audioSource;

    Coroutine currentColorLerp;
    Color defaultColor;

    private void Awake()
    {
        defaultColor = shipGraphics.color;
        audioSource = GetComponent<AudioSource>();
        playerColliders = GetComponentsInParent<Collider2D>();
    }

    private void OnEnable()
    {
        PlayerEvents.PlayerHit += OnPlayerHit;
        PlayerEvents.PlayerDefenseInput += ControlShield;
    }

    private void OnDisable()
    {
        PlayerEvents.PlayerHit -= OnPlayerHit;
        PlayerEvents.PlayerDefenseInput -= ControlShield;
    }

    IEnumerator ColorLerpCoroutine(Color targetColor)
    {
        Color currentColor = shipGraphics.color;
        float t = 0;
        while (shipGraphics.color != targetColor)
        {
            shipGraphics.color = Color.Lerp(currentColor, targetColor, t);
            t += Time.fixedDeltaTime * 5;
            yield return new WaitForEndOfFrame();
        }

        currentColorLerp = null;
    }

    private void ColorLerp(Color targetColor)
    {
        if (currentColorLerp != null)
        {
            StopCoroutine(currentColorLerp);
        }

        currentColorLerp = StartCoroutine(ColorLerpCoroutine(targetColor));
    }

    public void ControlShield(bool state)
    {
        foreach(Collider2D collider in playerColliders)
        {
            collider.enabled = !state;
        }

        if (state)
        {
            ColorLerp(phaseColor);

            audioSource.clip = phaseStartSfx;
            audioSource.Play();
        }
        else
        {
            ColorLerp(defaultColor);
        }
    }

    public void OnPlayerHit()
    {
        audioSource.clip = phaseDodgeSFX;
        audioSource.Play();
    }
}
