using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDefense : GunBase
{
    [SerializeField] float aimingRange;
    [SerializeField] float projectileLifetime;
    [SerializeField] float sprayMargin;

    GameObject currentTarget;
    Vector3 aimingDirection;
    int targetLayer;

    protected override void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        // Alterar a prefab diretamente altera TODOS os projeteis criados a partir da prefab alterada, logo,
        // é criado uma nova instância da prefab. Depois, essa instância é atualizada com os valores definidos na arma,
        // e com o layer de física adequado, definido de acordo com a tag da arma.
        projectilePrefab = Instantiate(defaultProjectilePrefab);
        ProjectileBase[] projectileScripts = projectilePrefab.GetComponentsInChildren<ProjectileBase>();

        foreach (ProjectileBase projectileScript in projectileScripts)
        {
            projectileScript.travelSpeed = projectileSpeed;
            projectileScript.damage = damagePerProjectile;
        }

        if (transform.CompareTag("Player"))
        {
            projectilePrefab.gameObject.layer = LayerMask.NameToLayer("PlayerPointDefenseProjectile");
            targetLayer = LayerMask.GetMask("EnemyProjectile");
        }
        else if (transform.CompareTag("Enemy"))
        {
            projectilePrefab.gameObject.layer = LayerMask.NameToLayer("EnemyPointDefenseProjectile");
            targetLayer = LayerMask.GetMask("PlayerProjectile");
        }   
        
        // Desativada pois ela é instânciada dentro da cena.
        projectilePrefab.SetActive(false); 
    }

    private void Update()
    {
        if (currentTarget == null)
        {
            // Procura um alvo, e, se for encontrado, pega a direção ao alvo
            Collider2D closestTarget = Physics2D.OverlapCircle(transform.position, aimingRange, targetLayer);
            if (closestTarget != null)
            {
                currentTarget = closestTarget.gameObject;
                SprayTowardsTarget();
            }
        }
        else
        {
            SprayTowardsTarget();
        }

        // Rotaciona para o alvo
        transform.rotation = Quaternion.LookRotation(Vector3.forward, aimingDirection);
    }

    private void SprayTowardsTarget()
    {
        Vector3 firingPoint = currentTarget.transform.position;
        firingPoint.x += Random.Range(-sprayMargin / 2, sprayMargin / 2);
        firingPoint.y += Random.Range(-sprayMargin / 2, sprayMargin / 2);

        Vector3 distanceToTarget = firingPoint - transform.position;
        distanceToTarget.z = 0;
        aimingDirection = distanceToTarget.normalized;
    }

    protected override IEnumerator FiringCoroutine()
    {
        while(true)
        {
            if (currentFiringTimer <= 1)
            {
                currentFiringTimer += Time.deltaTime * fireRate;
            }
            else if (currentFiringTimer >= 1 && GameplayManager.IsObjectOnScreen(transform.position))
            {
                if (currentTarget != null)
                {
                    Fire();
                    currentFiringTimer = 0;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    protected override void Fire()
    {
        base.Fire();

        Destroy(currentProjectileInstance, projectileLifetime);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, aimingRange);
    }
}
