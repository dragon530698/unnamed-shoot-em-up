using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Simples script que aplica uma força e rotação aleatória ao objeto.
public class DriftingObject : MonoBehaviour
{
    [SerializeField] float driftingForce;
    [SerializeField] float driftingTorque;
    private void OnEnable()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.AddForce(
            new Vector2(
                Random.Range(-driftingForce , driftingForce),
                Random.Range(-driftingForce, driftingForce)
            ),
            ForceMode2D.Impulse
        );

        rb.AddTorque(Random.Range(-driftingTorque, driftingTorque));
    }
}
