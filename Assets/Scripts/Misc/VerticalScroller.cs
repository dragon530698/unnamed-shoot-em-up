using UnityEngine;
using GameEvents;

// Simples script que movimenta todos os filhos do objeto para baixo, infinitamente.
public class VerticalScroller : MonoBehaviour
{
    [SerializeField] float scrollSpeed = 5;

    bool shouldScroll = true;

    private void OnEnable()
    {
        PlayerEvents.PlayerDeath += Deactivate;
    }

    private void OnDisable()
    {
        PlayerEvents.PlayerDeath -= Deactivate;
    }

    void Update()
    {
        if (shouldScroll)
        {
            foreach (Transform child in transform)
            {
                child.transform.Translate(new Vector3(0, -(scrollSpeed * Time.deltaTime), 0));
            }
        }
    }

    public void Deactivate()
    {
        shouldScroll = false;
    }
}
