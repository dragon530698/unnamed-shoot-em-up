using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;

public class BackgroundController : MonoBehaviour
{
    [SerializeField] List<GameObject> backgrounds = new List<GameObject>();
    [SerializeField] int scoreTransitionValue;

    [HideInInspector]
    public SpriteRenderer currentBackgroundRenderer;
    int currentBackground = 0;
    int lastBackground = 0;

    private void Awake()
    {
        currentBackground = ScoreManager.currentScore / scoreTransitionValue;


        if (currentBackground >= backgrounds.Count)
        {
            for (int i = 0; i < backgrounds.Count - 1; i++)
            {
                backgrounds[i].SetActive(false);
            }

            currentBackgroundRenderer = backgrounds.Last().GetComponent<SpriteRenderer>();
            return;
        }

        for (int i = 0; i <= currentBackground - 1; i++)
        {
            backgrounds[i].SetActive(false);
        }
        currentBackgroundRenderer = backgrounds[currentBackground].GetComponent<SpriteRenderer>();
        SetNewAlpha();
        lastBackground = currentBackground;
    }

    private void OnEnable()
    {
        ScoreEvents.ScoreGained += UpdateBackground;
    }

    private void OnDisable()
    {
        ScoreEvents.ScoreGained -= UpdateBackground;
    }

    public void UpdateBackground(int _)
    {
        currentBackground = ScoreManager.currentScore / scoreTransitionValue;
        
        if (currentBackground >= backgrounds.Count)
        {
            currentBackgroundRenderer = backgrounds.Last().GetComponent<SpriteRenderer>();
            return;
        }

        if (currentBackground != lastBackground)
        {
            currentBackgroundRenderer = backgrounds[currentBackground].GetComponent<SpriteRenderer>();

            for (int i = 0; i <= currentBackground - 1; i++)
            {
                backgrounds[i].SetActive(false);
            }
        }

        SetNewAlpha();

        lastBackground = currentBackground;
    }

    void SetNewAlpha()
    {
        if (currentBackground >= backgrounds.Count - 1) return;
        float newAlpha = ScoreManager.currentScore - (scoreTransitionValue * currentBackground);
        newAlpha /= scoreTransitionValue;
        newAlpha = 1 - newAlpha;

        currentBackgroundRenderer.color = new Color(1, 1, 1, newAlpha);
    }
}
