using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruct : MonoBehaviour
{
    [SerializeField] float destructDelay;

    private void Awake()
    {
        Destroy(this.gameObject, destructDelay);
    }
}
