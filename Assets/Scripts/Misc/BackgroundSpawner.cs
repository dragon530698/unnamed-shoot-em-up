using System.Collections.Generic;
using UnityEngine;

// Instancia e posiciona novos backgrounds baseados na prefab dada.
public class BackgroundSpawner : MonoBehaviour
{
    [SerializeField] GameObject backgroundPrefab;
    LinkedList<GameObject> createdBackgrounds = new LinkedList<GameObject>();

    void Start()
    {
        // Adiciona todos os backgrounds já criados á lista de backgrounds. 
        foreach (Transform child in transform)
        {
            createdBackgrounds.AddFirst(child.gameObject);
        }
    }

    void Update()
    {
        CreateOrDestroyBackground();
    }

    void CreateOrDestroyBackground()
    {
        float screenTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).y;
        float screenBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;

        GameObject lastBackground = createdBackgrounds.Last.Value;
        Bounds lastBackgroundBounds = lastBackground.GetComponent<BackgroundController>().currentBackgroundRenderer.bounds;

        GameObject firstBackground = createdBackgrounds.First.Value;
        Bounds firstBackgroundBounds = firstBackground.GetComponent<BackgroundController>().currentBackgroundRenderer.bounds;

        if (lastBackgroundBounds.max.y <= screenTop)
        {
            // Cria novo background, adiciona ele como filho e adiciona ele para a lista.
            GameObject newBackground = Instantiate(backgroundPrefab);
            newBackground.transform.parent = transform;
            createdBackgrounds.AddLast(newBackground);

            // Move o background novo para a posição do ultimo background, para depois move-lo
            // novamente para sua posição final (no caso, posição do ultimo background + tamanho y do sprite)
            newBackground.transform.localPosition = lastBackground.transform.localPosition;
            Bounds newBackgroundBounds = newBackground.GetComponent<BackgroundController>().currentBackgroundRenderer.bounds;
            newBackground.transform.Translate(new Vector3(0, newBackgroundBounds.size.y, 0));
        }

        // Caso o background já não é mais visto pela tela (considerando uma margem a mais),
        // ele é removido da lista e destruído.
        if (firstBackgroundBounds.max.y <= screenBottom * 1.5)
        {
            createdBackgrounds.RemoveFirst();
            Destroy(firstBackground);
        }
    }
}
